package com.appsfromholland.airportapp;

public class Airport {
    private String icao;
    private String name;
    private double longitude;
    private double latitude;
    private double elevation;
    private String iso_country;
    private String municipality;

    public Airport(String icao, String name, double longitude, double latitude, double elevation, String iso_country, String municipality) {
        this.icao = icao;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.elevation = elevation;
        this.iso_country = iso_country;
        this.municipality = municipality;
    }

    public String getIcao() {
        return icao;
    }

    public String getName() {
        return name;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getElevation() {
        return elevation;
    }

    public String getIso_country() {
        return iso_country;
    }

    public String getMunicipality() {
        return municipality;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "icao='" + icao + '\'' +
                ", name='" + name + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", elevation=" + elevation +
                ", iso_country='" + iso_country + '\'' +
                ", Municipality='" + municipality + '\'' +
                '}';
    }
}
