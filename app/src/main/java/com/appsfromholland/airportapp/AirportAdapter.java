package com.appsfromholland.airportapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;



public class AirportAdapter extends RecyclerView.Adapter<AirportAdapter.AirportViewHolder> {

    private ArrayList<Airport> dataset;

    public AirportAdapter(ArrayList<Airport> dataset) {
        this.dataset = dataset;
    }

    public class AirportViewHolder extends RecyclerView.ViewHolder {

        public TextView icao, name;

        public AirportViewHolder(View itemView) {
            super(itemView);
            icao = (TextView) itemView.findViewById(R.id.airport_icao);
            name = (TextView) itemView.findViewById(R.id.airport_name);
        }
    }

    @NonNull
    @Override
    public AirportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.airport_row_item, parent, false);
        return new AirportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AirportViewHolder holder, int position) {

        final Airport airport = dataset.get(position);
        holder.icao.setText(airport.getIcao());
        holder.name.setText(airport.getName());
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
