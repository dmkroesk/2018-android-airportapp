package com.appsfromholland.airportapp;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;
    AirportAdapter ad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AirportDatabaseFactory factory = AirportDatabaseFactory.getInstance(this);

        rv = findViewById(R.id.airportsRecyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));
        ad = new AirportAdapter(factory.getAirports(false));
        rv.setAdapter(ad);


    }
}
